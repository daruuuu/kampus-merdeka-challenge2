const checkEmail = (email) => {
  const mailFormat1 =
    /^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8}(\.[a-z]{2,8})?$)/;
  const mailFormat2 = /^([a-z\d\.-]+)@([a-z\d-]+)/;
  const mailFormat3 = /^([a-z\d\.-]+)/;

  if (email == "" || email == null) {
    return "Error: Please input something";
  }
  if (typeof email !== "string") {
    return "Invalid data type";
  }
  if (email.match(mailFormat1)) {
    return "Valid";
  }
  if (email.match(mailFormat2)) {
    return "Invalid";
  }
  if (email.match(mailFormat3)) {
    return "Error: @(example) missing!";
  }
};

console.log(checkEmail("apranata@binar.co.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
console.log(checkEmail(3322));
console.log(checkEmail());
