const getAngkaTerbesarKedua = (dataNumbers) => {
  if (dataNumbers == null) {
    return "Error: You need to input something";
  }
  if (dataNumbers == "") {
    return "Error: Please input something more";
  }
  const arr = Array.from(new Set(dataNumbers)).sort((a, b) => a - b);
  return arr[arr.length - 2];
};

const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8];

console.log(getAngkaTerbesarKedua(dataAngka));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
