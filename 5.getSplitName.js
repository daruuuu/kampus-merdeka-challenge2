const getSplitName = (personName) => {
  if (typeof personName == "undefined") {
    return "Error: Please input something";
  }
  if (typeof personName != "string") {
    return "Error: Invalid data type";
  }
  const arr = personName.split(" ");
  const [firstName, middleName, lastName] = arr;
  const obj = { firstName, middleName, lastName };
  switch (arr.length) {
    case 3:
      return obj;
    case 2:
      obj.middleName = null;
      obj.lastName = arr[1];
      return obj;
    case 1:
      obj.middleName = null;
      obj.lastName = null;
      return obj;
    default:
      return "Error: items in array must be 1 - 3";
  }
};

console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));
console.log(getSplitName(0));
console.log(getSplitName());
