const isValidPassword = (givenPassword) => {
  let passFormat = /^(?=^.{8,}$)(?=.*[A-Z])(?=.*[a-z])(?=.*\d).*$/;
  if (givenPassword == null) {
    return "Error: Please input something";
  }
  if (typeof givenPassword !== "string") {
    return "Invalid data type";
  }
  if (givenPassword.match(passFormat)) {
    return true;
  }
  return false;
};

console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword("Meong2"));
console.log(isValidPassword(0));
console.log(isValidPassword());
