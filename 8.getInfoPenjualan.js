const dataPenjualanNovel = [
  {
    idProduct: "BOOK002421",
    namaProduk: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: "BOOK002351",
    namaProduk: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

const getInfoPenjualan = (dataPenjualan) => {
  let obj = {
    totalKeuntungan: 0,
    totalModal: 0,
    persentaseKeuntungan: "0",
    produkBukuTerlaris: "",
    penulisTerlaris: "",
  };

  const rupiah = (uang) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(uang);
  };

  const untung = dataPenjualan.reduce(
    (a, b) => a + (b.hargaJual - b.hargaBeli) * b.totalTerjual,
    0
  );

  const modal = dataPenjualan.reduce(
    (a, b) => a + (b.sisaStok + b.totalTerjual) * b.hargaBeli,
    0
  );

  const persentaseUntung = () => ((untung / modal) * 100).toFixed(2) + "%";

  const bukuLaris = () => {
    return dataPenjualan.reduce((a, b) =>
      a.totalTerjual > b.totalTerjual ? a : b
    ).namaProduk;
  };

  const penulisLaris = () => {
    const holder = {};
    dataPenjualanNovel.forEach(function (d) {
      if (holder.hasOwnProperty(d.penulis)) {
        holder[d.penulis] = holder[d.penulis] + d.totalTerjual;
      } else {
        holder[d.penulis] = d.totalTerjual;
      }
    });

    const arr = [];
    for (var prop in holder) {
      arr.push({ penulis: prop, totalTerjual: holder[prop] });
    }
    return arr.reduce((a, b) => (a.totalTerjual > b.totalTerjual ? a : b))
      .penulis;
  };

  obj.totalKeuntungan = rupiah(untung);
  obj.totalModal = rupiah(modal);
  obj.persentaseKeuntungan = persentaseUntung();
  obj.produkBukuTerlaris = bukuLaris();
  obj.penulisTerlaris = penulisLaris();

  return obj;
};

console.log(getInfoPenjualan(dataPenjualanNovel));
